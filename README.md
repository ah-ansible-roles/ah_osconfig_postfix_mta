# ah_osconfig_postfix_mta

An Ansible role to configure the Postfix MTA to send emails via GMail.

Requires an app password configured in GMail as well as legacy login.

This role **does** use my custom Ansible Vault credentials.

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| Fedora | Y | WORKING |
| Raspberry Pi OS | Y | WORKING |
| RHEL | Y | WORKING |
| Rocky Linux | Y | WORKING |
| Ubuntu | Y | WORKING |

## Changelog

| **Date** | **Description** |
|---|---|
| 2022-01-03 | Expand support to whole Debian family instead of just Ubuntu. |
| 2022-01-02 | Only restart Postfix if we need to, and fix a bug with Ubuntu config getting pushed to Red Hat. |
| 2021-12-19 | Tested and working across multiple distributions. |
